# gameData_module.py

from flask import Flask, jsonify, request
from flask.views import MethodView
from DB.db_module import db
import json
from sqlalchemy.dialects.postgresql import ARRAY
import datetime
from .compare_module import updateDamgageCompare, deleteDamgageCompare, getDamageCompareList

class GameDataList(MethodView):
    def get(self):
        search = request.values.get('search')
        gameDataList = getGameDataList(search)
        if gameDataList == False:
            response = jsonify(message='get GameDataList Error!')
            response.status_code = 400
            return response
        else:
            dumpList = []
            for o in gameDataList:
                dumpList.append(o.dump())

            response = jsonify(dumpList)
            response.status_code = 200
            return response

    
class GameData(MethodView):
    def post(self):
        data = json.loads(request.data)
        if data == None:
            response = jsonify(message='GameData is empty!')
            response.status_code = 400
            return response

        statue = addGameData(data)
        if statue['code'] == 200:
            response = jsonify(message='commit GameDataList success!')
            response.status_code = 200
            return response
        else:
            response = jsonify(message=statue['msg'])
            response.status_code = statue['code']
            return response
        
    def delete(self):
        ids = request.args.getlist('roundIds[]')
        roundIds = []
        for id in ids:
            roundIds.append(int(id))
        statue = deleteGameInfo(roundIds)
        if statue['code'] == 200:
            response = jsonify(message='delete GameDataList success!')
            response.status_code = 200
            return response
        else:
            response = jsonify(message=statue['msg'])
            response.status_code = statue['code']
            return response

class CompareData(MethodView):
    def get(self):
            playerId = request.values.get('playerId')
            gameId = request.values.get('gameId')
            if(playerId == None) or (gameId == None):
                response = jsonify(message='get playerId or gameId is null !')
                response.status_code = 400
                return response
            compareDataList = getDamageCompareList(int(gameId), int(playerId))
            if compareDataList == False:
                response = jsonify(message='get CompareDataList Error!')
                response.status_code = 400
                return response
            else:
                response = jsonify(compareDataList)
                response.status_code = 200
                return response

class GameDataInfo(db.Model):
    __tablename__ = 'GameDataList'
    gameId = db.Column(db.Integer, nullable=False)
    roundId = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime)
    # table relationship bind child table
    playerDataList = db.relationship('PlayerData') 

    def __init__(self, data):
        self.gameId = data['gameId']
        now = datetime.datetime.now()
        self.date = now
        _list = data['playerDataList']

        for player in _list:
            playerData = PlayerData(player)
            self.playerDataList.append(playerData)
        
    def __repr__(self):
        return '<GameDataInfo %r>' % self.roundId

    def dump(self):
        array = []
        for data in self.playerDataList:
            array.append(data.dump())
        return {
            'roundId': self.roundId,
            'date': self.date,
            'gameId': self.gameId,
            'playerDataList': array,
        }

class PlayerData(db.Model):
    __tablename__ = 'PlayerDataList'
    # table ForeignKey bind to main table
    roundId = db.Column(db.Integer, db.ForeignKey('GameDataList.roundId'))
    id = db.Column(db.Integer, primary_key=True)
    playerId = db.Column(db.Integer, nullable=False)
    damageRatio = db.Column(db.Integer, nullable=False)

    def __init__(self, data):
        self.playerId = data['playerId']
        self.damageRatio = data['damageRatio']
    def __repr__(self):
        return '<PlayerData %r>' % self.playerId
    def dump(self):
        return {
            'playerId': self.playerId,
            'damageRatio': self.damageRatio,
        }

def addGameData(gameData):
    gameDataInfo = GameDataInfo(gameData)
    status = updateDamgageCompare(gameDataInfo)
    if status['code'] != 200:
        return status
    try:
        db.session.add(gameDataInfo)
        db.session.commit()
        return { 'code': 200}
    except:
        return { 'code': 400, 'msg': 'GameDataInfo commit error'}

def getGameDataList(search = ''):
    try:
        if(search == ''):
            gameDataList = GameDataInfo.query.all()
        else:
            gameId = int(search)
            gameDataList = db.session.query(GameDataInfo).filter(GameDataInfo.gameId == gameId).all()

        return gameDataList
    except:
        print('-------- getGameDataList error -----------')
        return False

def deleteGameInfo(roundIds):
    try:
        for roundId in roundIds:
            data = db.session.query(GameDataInfo).filter(GameDataInfo.roundId == roundId).first()
            if data != None:
                status = deleteDamgageCompare(data)
                if status['code'] != 200:
                    return status
                db.session.query(GameDataInfo).filter(GameDataInfo.roundId == roundId).delete()
                db.session.query(PlayerData).filter(PlayerData.roundId == roundId).delete()
        db.session.commit()
        return { 'code': 200 }
    except:
        return { 'code': 400, 'msg': 'delete GameDataInfo or PlayerData error'}