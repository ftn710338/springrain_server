from flask import Flask, jsonify, request
from flask.views import MethodView
from sqlalchemy import Integer
from DB.db_module import db
from .player_module import getPlayerList
import json

def updateDamgageCompare(data):
    gameId = data.gameId
    dataList = []
    for data in data.playerDataList:
        dataList.append(data.dump())

    damageCompareList = []

    for i in range(0, len(dataList)):
        if i+1 < len(dataList):
            for n in range(i+1, len(dataList)):
                damageCompareList.append(DamageCompare(dataList[i], dataList[n]))

    for compare in damageCompareList:
        newData = DamageCompareSum(gameId, compare.p1, compare.p2, compare.compareRatio, 1)
        oldData = getDamageCompareData(gameId, compare.p1, compare.p2)
        if oldData == False:
            return { 'code': 400, 'msg': 'getDamageCompareData error'}
        else:
            if oldData == None:
                try:
                    db.session.add(newData)
                    db.session.commit()
                except:
                    return  { 'code': 400, 'msg': 'commit DamageCompareSum error'}
            else:
                ratio = sumRatio(newData, oldData)
                try:
                    count = newData.count + oldData.count
                    data = db.session.query(DamageCompareSum).filter(DamageCompareSum.dataId == oldData.dataId).update({DamageCompareSum.compareRatio: ratio, DamageCompareSum.count: count })
                except:
                    return  { 'code': 400, 'msg': 'commit DamageCompareSum error'}
    
    return { 'code': 200 }

def deleteDamgageCompare(data):
    gameId = data.gameId
    dataList = []
    for data in data.playerDataList:
        dataList.append(data.dump())

    damageCompareList = []

    for i in range(0, len(dataList)):
        if i+1 < len(dataList):
            for n in range(i+1, len(dataList)):
                damageCompareList.append(DamageCompare(dataList[i], dataList[n]))
    
    for compare in damageCompareList:
        newData = DamageCompareSum(gameId, compare.p1, compare.p2, compare.compareRatio, 1)
        oldData = getDamageCompareData(gameId, compare.p1, compare.p2)
        if oldData == False:
            return { 'code': 400, 'msg': 'getDamageCompareData error'}
        else:
            if oldData != None:
                if oldData.count == 1:
                    try:
                        db.session.query(DamageCompareSum).filter(DamageCompareSum.dataId == oldData.dataId).delete()
                        db.session.commit()
                    except:
                        return  { 'code': 400, 'msg': 'delete DamageCompareSum error'}
                else :
                    ratio = reduceRation(newData, oldData)
                    try:
                        count = oldData.count - newData.count
                        data = db.session.query(DamageCompareSum).filter(DamageCompareSum.dataId == oldData.dataId).update({DamageCompareSum.compareRatio: ratio, DamageCompareSum.count: count })
                    except:
                        return  { 'code': 400, 'msg': 'delete DamageCompareSum error'}
    
    return { 'code': 200 }

def getDamageCompareList(gameId, playerId):
    try:
        playerList = getPlayerList('')
        playerIds = []

        for player in playerList:
            data = player.dump()
            playerIds.append(data['playerId'])

        playerIds.remove(playerId)

        sumData = {
            'gameId' : gameId,
            'playerId' : playerId,
            'comparePlayer': []
        }
        dataP1 = db.session.query(DamageCompareSum).filter(DamageCompareSum.gameId == gameId, DamageCompareSum.p1 == playerId).all()

        for data in dataP1:
            paserData = {
                'playerId' : data.p2,
                'compareRatio': round( 1 / data.compareRatio, 2)
            }
            sumData['comparePlayer'].append(paserData)
        
        dataP2 = db.session.query(DamageCompareSum).filter(DamageCompareSum.gameId == gameId, DamageCompareSum.p2 == playerId).all()

        for data in dataP2:
            paserData = {
                'playerId' : data.p1,
                'compareRatio': round(data.compareRatio, 2)
            }
            sumData['comparePlayer'].append(paserData)

        newAdd = True

        if(len(sumData['comparePlayer']) > 0):
            while(len(playerIds) > 0):
                if(newAdd == True):
                    newAdd = False
                    for comparePlayer in sumData['comparePlayer']:
                        id = comparePlayer['playerId']
                        ratio = comparePlayer['compareRatio']
                        isId = id in playerIds
                        if(isId == True):
                            playerIds.remove(id)
                            dataP1 = db.session.query(DamageCompareSum).filter(DamageCompareSum.gameId == gameId, DamageCompareSum.p1 == id).all()

                            for data in dataP1:
                                isAdded = False
                                for d in sumData['comparePlayer']:
                                    if d['playerId'] == data.p2 or data.p2 == playerId:
                                        isAdded = True

                                if isAdded == False:
                                    paserData = {
                                        'playerId' : data.p2,
                                        'compareRatio':  round((1 / data.compareRatio) * ratio, 2)
                                    }
                                    sumData['comparePlayer'].append(paserData)
                                    newAdd = True
                            
                            dataP2 = db.session.query(DamageCompareSum).filter(DamageCompareSum.gameId == gameId, DamageCompareSum.p2 == id).all()

                            for data in dataP2:
                                isAdded = False
                                for d in sumData['comparePlayer']:
                                    if d['playerId'] == data.p1 or data.p1 == playerId:
                                        isAdded = True
                                if isAdded == False:
                                    paserData = {
                                        'playerId' : data.p1,
                                        'compareRatio': round(data.compareRatio * ratio, 2)
                                    }
                                    sumData['comparePlayer'].append(paserData)
                                    newAdd = True
                    else:
                        break

        return sumData
    except:
        return False


class DamageCompare():
    p1 = 0
    p2 = 0
    compareRatio = 0
    def __init__(self, data1, data2):
        p1 = None
        p2 = None
        if data1['playerId'] < data2['playerId']:
            p1 = data1
            p2 = data2
        else:
            p1 = data2
            p2 = data1

        self.p1 = p1['playerId']
        self.p2 = p2['playerId']
        self.compareRatio = float(p1['damageRatio'])/float(p2['damageRatio'])
    def dump(self):
        return {
            'p1': self.p1,
            'p2': self.p2,
            'compareRatio': round(self.compareRatio, 2),
        }

class DamageCompareSum(db.Model):
    __tablename__ = 'DamageCompareSum'

    dataId = db.Column(db.Integer, primary_key=True)
    gameId = db.Column(db.Integer, nullable=False)
    p1 = db.Column(db.Integer, nullable=False)
    p2 = db.Column(db.Integer, nullable=False)
    compareRatio = db.Column(db.Integer, nullable=False)
    count = db.Column(db.Integer, nullable=False)

    def __init__(self, gameId, p1, p2, ratio, count):
        self.gameId = gameId
        self.p1 = p1
        self.p2 = p2
        self.compareRatio = ratio
        self.count = count
    def dump(self):
        return {
            'gameId': self.gameId,
            'p1': self.p1,
            'p2': self.p2,
            'compareRatio': self.compareRatio,
            'count': self.count,
        }

def getDamageCompareData(gameId, p1, p2):
    try:
        oldData = db.session.query(DamageCompareSum).filter(DamageCompareSum.gameId == gameId, DamageCompareSum.p1 == p1, DamageCompareSum.p2 == p2).first()
        return oldData
    except:
        return False

def sumRatio(newData, oldData):
    new_p1Damage = newData.compareRatio * newData.count
    new_p2Damage = 1 * newData.count
    old_p1Damage = oldData.compareRatio * oldData.count
    old_p2Damage = 1 * oldData.count
    ratio = (old_p1Damage + new_p1Damage) / (old_p2Damage + new_p2Damage)
    return round(ratio, 2)

def reduceRation(newData, oldData):
    new_p1Damage = newData.compareRatio * newData.count
    new_p2Damage = 1 * newData.count
    old_p1Damage = oldData.compareRatio * oldData.count
    old_p2Damage = 1 * oldData.count
    ratio = (old_p1Damage - new_p1Damage) / (old_p2Damage - new_p2Damage)
    return round(ratio, 2)

