# player_module.py

from flask import Flask, jsonify, request
from flask.views import MethodView
from DB.db_module import db
import json

class PlayerList(MethodView):
    def get(self):
        search = request.values.get('search')
        playerList = getPlayerList(search)
        if playerList == False:
            response = jsonify(message='get PlayerList Error!')
            response.status_code = 400
            return response
        else:
            dumpList = []
            for o in playerList:
                dumpList.append(o.dump())

            resp = {
                'rows': dumpList,
                'total': len(dumpList)
            }

            response = jsonify(resp)
            response.status_code = 200
            return response
            

class Player(MethodView):
    def post(self):
        playerName = request.values.get('playerName')
        if playerName == None:
            response = jsonify(message='playerName is empty!')
            response.status_code = 400
            return response

        statue = addPlayer(playerName)
        if statue == 200:
            response = jsonify(message='commit PlayerList success!')
            response.status_code = 200
            return response
        elif statue == 409:
            response = jsonify(message='commit PlayerList same player error!')
            response.status_code = 409
            return response
        else:
            response = jsonify(message='commit PlayerList error!')
            response.status_code = 400
            return response
    def put(self):
        playerId = request.values.get('playerId')
        playerName = request.values.get('playerName')
        if (playerName is None) or (playerId is None):
            response = jsonify(message='playerName or playerId is empty!')
            response.status_code = 400
            return response

        statue = editPlayer(playerId, playerName)
        if statue == 200:
            response = jsonify(message='commit PlayerList success!')
            response.status_code = 200
            return response
        elif statue == 409:
            response = jsonify(message='this player does not in PlayerList !')
            response.status_code = 409
            return response
        else:
            response = jsonify(message='commit PlayerList error!')
            response.status_code = 400
            return response
        
        
    def delete(self):
        ids = request.args.getlist('playerIds[]')

        playerIds = []
        for id in ids:
            playerIds.append(int(id))

        statue = deletePlayer(playerIds)
        if statue == 200:
            response = jsonify(message='delete PlayerList success!')
            response.status_code = 200
            return response
        elif statue == 409:
            response = jsonify(message='delete PlayerList same player error!')
            response.status_code = 409
            return response
        else:
            response = jsonify(message='delete PlayerList error!')
            response.status_code = 400
            return response

class PlayerInfo(db.Model):
    __tablename__ = 'PlayerList'
    playerId = db.Column(db.Integer, primary_key=True)
    playerName = db.Column(db.String(120), unique=True, nullable=False)

    def __init__(self, playerName):
        self.playerName = playerName

    def __repr__(self):
        return '<PlayerInfo %r>' % self.playerName
    def dump(self):
        return {
            'playerId': self.playerId,
            'playerName': self.playerName,
        }

def addPlayer(playerName):
    player = PlayerInfo(playerName)
    
    try:
        playerList = getPlayerList()
        for playerInfo in playerList:
            if playerInfo.playerName == playerName:
                return 409
        db.session.add(player)
        db.session.commit()
        return 200
    except:
        return 400

def editPlayer(playerId, playerName):
    try:
        playerList = getPlayerList()
        for playerInfo in playerList:
            if playerInfo.playerName == playerName:
                return 409
        player = db.session.query(PlayerInfo).filter(PlayerInfo.playerId == playerId).first()
        if player != None:
            db.session.query(PlayerInfo).filter(PlayerInfo.playerId == playerId).update({PlayerInfo.playerName: playerName })
            db.session.commit()
        
        return 200
    except:
        return 400

def deletePlayer(playerIds):
    try:
        if(len(playerIds) > 0):
            for playerId in playerIds:
                db.session.query(PlayerInfo).filter(PlayerInfo.playerId == playerId).delete()
            db.session.commit()
        return 200
    except:
        return 400

def getPlayerList(search = ''):
    try:
        if(search == ''):
            playerList = PlayerInfo.query.all()
        elif(search.isnumeric()):
            id = int(search)
            playerList = db.session.query(PlayerInfo).filter(PlayerInfo.playerId == id).all()
        else:
            name = search
            playerList = db.session.query(PlayerInfo).filter(PlayerInfo.playerName == name).all()
        return playerList
    except:
        print('-------- getPlayerList error -----------')
        return False
