from flask import Flask, jsonify
import json
from DB.db_module import db, app

from flask_cors import CORS, cross_origin
from api.game_module import GameList, Game
from api.player_module import PlayerList, Player
from api.gameData_module import GameDataList, GameData, CompareData

# CORS(app, support_credentials=True)
CORS(app)
app.add_url_rule('/getPlayerList/', view_func=PlayerList.as_view('getPlayerList'))
app.add_url_rule('/player/', view_func=Player.as_view('player'))
app.add_url_rule('/getGameList/', view_func=GameList.as_view('getGameList'))
app.add_url_rule('/game/', view_func=Game.as_view('game'))
app.add_url_rule('/getGameDataList/', view_func=GameDataList.as_view('getGameDataList'))
app.add_url_rule('/gameData/', view_func=GameData.as_view('gameData'))
app.add_url_rule('/getCompareDataList/', view_func=CompareData.as_view('getCompareDataList'))

if __name__ == '__main__':
    db.create_all()
    # app.run(host="0.0.0.0", port=int("8081"), debug=True)
    app.run(host="0.0.0.0", port=int("8081"))
