# game_module.py

from flask import Flask, jsonify, request
from flask.views import MethodView
from DB.db_module import db
import json

class GameList(MethodView):
    def get(self):
        gameList = getGameList()
        if gameList == False:
            response = jsonify(message='get gameList Error!')
            response.status_code = 400
            return response
        else:
            dumpList = []
            for o in gameList:
                dumpList.append(o.dump())
            resp = {
                'rows': dumpList,
                'total': len(dumpList)
            }

            response = jsonify(dumpList)
            response.status_code = 200
            return response

    
class Game(MethodView):
    def post(self):
        gameName = request.values.get('gameName')
        if gameName == None:
            response = jsonify(message='gameName is empty!')
            response.status_code = 400
            return response

        statue = addGame(gameName)
        if statue == 200:
            response = jsonify(message='commit gameList success!')
            response.status_code = 200
            return response
        elif statue == 409:
            response = jsonify(message='commit gameList same game error!')
            response.status_code = 409
            return response
        else:
            response = jsonify(message='commit gameList error!')
            response.status_code = 400
            return response
    def put(self):
        gameId = request.values.get('gameId')
        gameName = request.values.get('gameName')
        if (gameId == None) or (gameName == None):
            response = jsonify(message='gameId or gameName is empty!')
            response.status_code = 400
            return response

        statue = editGame(gameId, gameName)
        if statue == 200:
            response = jsonify(message='commit GameList success!')
            response.status_code = 200
            return response
        elif statue == 409:
            response = jsonify(message='this game does not in GameList !')
            response.status_code = 409
            return response
        else:
            response = jsonify(message='commit GameList error!')
            response.status_code = 400
            return response
        
    def delete(self):
        ids = request.args.getlist('gameIds[]')
        gameIds = []
        for id in ids:
            gameIds.append(int(id))
        statue = deleteGame(gameIds)
        if statue == 200:
            response = jsonify(message='delete gameList success!')
            response.status_code = 200
            return response
        elif statue == 409:
            response = jsonify(message='delete gameList same game error!')
            response.status_code = 409
            return response
        else:
            response = jsonify(message='delete gameList error!')
            response.status_code = 400
            return response

class GameInfo(db.Model):
    __tablename__ = 'GameList'
    gameId = db.Column(db.Integer, primary_key=True)
    gameName = db.Column(db.String(120), unique=True, nullable=False)

    def __init__(self, gameName):
        self.gameName = gameName

    def __repr__(self):
        return '<GameInfo %r>' % self.gameName
    def dump(self):
        return {
            'gameId': self.gameId,
            'gameName': self.gameName,
        }

def addGame(gameName):
    game = GameInfo(gameName)
    
    try:
        gameList = getGameList()
        for gameInfo in gameList:
            if gameInfo.gameName == gameName:
                return 409
        db.session.add(game)
        db.session.commit()
        return 200
    except:
        return 400
def editGame(gameId, gameName):

    try:
        gameList = getGameList()
        for gameInfo in gameList:
            if gameInfo.gameName == gameName:
                return 409
        game = db.session.query(GameInfo).filter(GameInfo.gameId == gameId).first()
        if game != None:
            db.session.query(GameInfo).filter(GameInfo.gameId == gameId).update({GameInfo.gameName: gameName })
            db.session.commit()
        return 200
    except:
        return 400

def deleteGame(gameIds):
    try:
        for gameId in gameIds:
            db.session.query(GameInfo).filter(GameInfo.gameId == gameId).delete()
        db.session.commit()
        return 200
    except:
        return 400

def getGameList():
    try:
        gameList = GameInfo.query.all()
        return gameList
    except:
        print('-------- getGameList error -----------')
        return False
